/**
 * Relay
 *
 * Controla um sistema externo mediante a leitura de comandos enviados pela porta série.
 * Neste exemplo o relé vai ser utilizado como interruptor para ligar ou desligar um sistema externo. O controlo da abertura ou fecho do relé
 * é feito mediante o envio de comandos através da porta série:
 *
 *    0 - desarma o relé interrompendo o circuito externo;
 *    1 - arma o relé ligando o circuito externo.
 *
 * criado a 28 de Abril 2016
 * por Bruno Horta, André Rosa
 */

#define PIN_RELAY 13

void setup() {
	Serial.begin(9600);
	/**
	 * Definir o modo de funcionamento do pino de controlo do relé
	 */
	pinMode(PIN_RELAY, OUTPUT);
}

void loop() {
	/**
	 * Caso hajam dados na porta série para serem lidos
	 */
	if (Serial.available()) {
		/**
		 * Le os dados
		 */
		int read = Serial.parseInt();

		/**
		 * Faz o reconhecimento do comando
		 */
		if (read == 1) {
			/**
			 * Comando reconhecido como 1, arma o relé ligando o circuito externo
			 */
			digitalWrite(PIN_RELAY, HIGH);
			Serial.println("Ligado");

		} else if (read == 0) {
			/**
			 * Comando reconhecido como 0, desarma o relé interrompendo o circuito externo
			 */
			digitalWrite(PIN_RELAY, LOW);
			Serial.println("Desligado");

		} else {
			/**
			 * Comando não reconhecido, não faz nada
			 */
			Serial.println("Comando não reconhecido");
		}

		Serial.println(read);
	}
}